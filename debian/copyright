Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: namebench
Upstream-Contact: Thomas Strömberg <tstromberg@google.com>
Source: http://code.google.com/p/namebench/
Comment: The upstream tarball has been modified as follows, to comply with DFSG:
 * Directory nb_third_party was removed since it contained all the dependences
   needed to build and run namebench. As Debian Policy dictates, those
   dependences were listed as Depends and/or Build-Depends.
 .
 * Directory cocoa has been removed since it contained binary files not
   useful at build time (those file are only needed to build namebench
   on Mac OS X. Also, this folder contains an unlicensed, undistributable,
   and unmodifieable file (cocoa/main.m).

Files: *
Copyright: © 2009-2010, Google Inc.
License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
     /usr/share/common-licenses/Apache-2.0 (on Debian systems)
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

Files: debian/*
Copyright: © 2010-2011, Miguel Landaeta <miguel@miguel.cc>
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 The full text of the GPL is distributed in /usr/share/common-licenses/GPL-2
 on Debian systems.
