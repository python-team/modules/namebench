namebench (1.3.1+dfsg-3) UNRELEASED; urgency=low

  [ Miguel Landaeta ]
  * Install .menu and .desktop files. (Closes: #635338).
    (Thanks to Martintxo <martintxo@sindominio.net>)
  * Bump Standards-Version to 3.9.3. No changes were required.
  * Update copyright file.
  * Fix watch file.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Jakub Wilk <jwilk@debian.org>  Sun, 05 May 2013 16:01:20 +0200

namebench (1.3.1+dfsg-2) unstable; urgency=low

  * Switch from dh_pysupport to dh_python2.
  * Simplify rules file.
  * Replace Build-Depends on python with python-all (>= 2.6.6-3~).
  * Bump Standards-Version to 3.9.2. No changes were required.
  * Replace docbook-to-man with rst2man as tool to generate the manpage.
  * Python transition:
    - Add support for python2.7.
    - Drop support for python2.5.
  * Decode GeoIP results with the correct encoding to avoid crashes.
    (Closes: #594146).
  * Drop versioned dependencies since they are satisfied even in stable.
  * Make copyright file DEP-5 compliant.
  * Move comments from README.source to copyright file.
  * Simplify watch file.
  * Small fixes in Description and manpage.
  * Replace XS-P-V deprecated field with X-P-V.
  * Install libnamebench as private module.

 -- Miguel Landaeta <miguel@miguel.cc>  Sun, 24 Apr 2011 18:43:14 -0430

namebench (1.3.1+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Updated watch file.
  * Switched source package format to 3.0 (quilt).
  * Dropped unneeded version for dependence on python (>= 2.5).
  * Added necessary version for dependence on python-dnspython (>= 1.8.0).
  * Updated manpage.

 -- Miguel Landaeta <miguel@miguel.cc>  Thu, 24 Jun 2010 09:23:04 -0430

namebench (1.2+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Updated watch file and added debian/orig-tar.sh.
  * debian/rules: Added get-orig-source target.
  * Update years in copyright file.
  * Configuration files are now installed in /etc/namebench directory.
  * Removed unnecessary files from /usr/share/doc/namebench directory.
  * Updated XS-Python-Version, Build-Depends and Depends to python >= 2.5
    since this version doesn't work with 2.4 anymore due to the use of
    relative imports.

 -- Miguel Landaeta <miguel@miguel.cc>  Sun, 07 Mar 2010 13:32:50 -0430

namebench (1.1+dfsg-2) unstable; urgency=low

  * Fixed FTBFS with Python 2.6 as default. (Closes: #571551).
  * Put namebench in net section. (Closes: #571114).
  * Bumped Standards-Version to 3.8.4. No changes were needed.
  * Removed public copy of namebench.py and /usr/bin/namebench
    was symlinked to the private one at /usr/share/namebench.

 -- Miguel Landaeta <miguel@miguel.cc>  Sun, 28 Feb 2010 12:17:54 -0430

namebench (1.1+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #559939).

 -- Miguel Landaeta <miguel@miguel.cc>  Wed, 27 Jan 2010 19:49:17 -0430
