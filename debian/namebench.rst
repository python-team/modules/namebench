===========
 namebench
===========

---------------------------------
open-source DNS benchmark utility
---------------------------------

:Author: Thomas Strömberg <tstromberg@google.com>
:Date:   2010-11-20
:Manual section: 1

SYNOPSIS
========

**namebench [nameserver1 ... [nameserverN]]**

DESCRIPTION
===========

namebench searches the fastest DNS servers available for your computer
to use. namebench runs a fair and thorough benchmark using your web
browser history, tcpdump output, or standardized datasets in order to
provide an individualized recommendation.


OPTIONS
=======

This program follows the usual GNU command line syntax, with long
options starting with two dashes (-). A summary of options is
included below.  For a complete description, see the README file
or visit the project's website.

-h, --help  Show summary of options.
-r RUN_COUNT, --runs=RUN_COUNT  Number of test runs to perform on each nameserver.
-z CONFIG, --config=CONFIG  Configuration file to use.
-o OUTPUT_FILE, --output=OUTPUT_FILE  Filename to write output to.
-t TEMPLATE, --template=TEMPLATE  Template to use for output generation (ascii, html, resolv.conf).
-c CSV_FILE, --csv_output=CSV_FILE  Filename to write query details to (CSV).
-j HEALTH_THREAD_COUNT, --threads=HEALTH_THREAD_COUNT  Number of health check threads to use.
-J BENCHMARK_THREAD_COUNT, --threads=BENCHMARK_THREAD_COUNT  Number of benchmark threads to use.
-P PING_TIMEOUT, --ping_timeout=PING_TIMEOUT  Number of seconds ping requests timeout in.
-y TIMEOUT, --timeout=TIMEOUT  Number of seconds general requests timeout in.
-Y HEALTH_TIMEOUT, --health_timeout=HEALTH_TIMEOUT  Health check timeout (in seconds).
-i INPUT_SOURCE, --import=INPUT_SOURCE  Import hostnames from an filename or application (alexa, cachehit, cachemiss, cachemix, camino, chrome, chromium, epiphany, firefox, flock, galeon, icab, internet_explorer, konqueror, midori, omniweb, opera, safari, seamonkey, squid, sunrise).
-I, --invalidate_cache  Force health cache to be invalidated.
-q QUERY_COUNT, --tests=QUERY_COUNT  Number of queries per run.
-m SELECT_MODE, --select_mode=SELECT_MODE  Selection algorithm to use (weighted, random, chunk).
-s NUM_SERVERS, --num_servers=NUM_SERVERS  Number of nameservers to include in test.
-S, --system_only  Only test the currently configured system nameservers.
-w, --open_webbrowser  Opens the final report in your browser.
-u, --upload_results  Upload anonmyized results to SITE_URL (default: False).
-U SITE_URL, --site_url=SITE_URL  URL to upload results to (default: http://namebench.appspot.com/).
-H, --hide_results  Upload results, but keep them hidden from indexes.
-x, --no_gui  Disable GUI.
-C, --enable-censorship-checks  Enable censorship checks.
-6, --ipv6_only  Only include IPv6 name servers.
-O, --only  Only test nameservers passed as arguments.

COPYRIGHT
=========
This manual page was written by Miguel Landaeta <miguel@miguel.cc> for the
Debian system (but may be used by others). Permission is granted to copy,
distribute and/or modify this document under the terms of the terms of GNU
General Public License, Version 2 or any later version published by the Free
Software Foundation. On Debian systems, the complete text of the GNU General
Public License can be found in /usr/share/common-licenses/GPL.

SEE ALSO
========

**dig(1)**, **host(1)**.
