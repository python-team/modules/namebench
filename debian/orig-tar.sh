#!/bin/sh

set -x

# called by uscan with '--upstream-version' <version> <file>
echo "version $2"
package=`dpkg-parsechangelog | sed -n 's/^Source: //p'`
debian_version=`dpkg-parsechangelog | sed -ne 's/^Version: \(.*+dfsg\)-.*/\1/p'`
TAR="$package"_${debian_version}.orig.tar.gz
DIR=$package-${debian_version}.orig

# clean up the upstream tarball
tar -zxvf $3 && mv $package-$2 $DIR
GZIP=--best tar -c -z -f $TAR --exclude 'cocoa' --exclude 'nb_third_party' $DIR
rm -rf $3 $DIR
